package auth

import (
	"fmt"
	"net/url"
)

func NewFromURL(authURL string) (AuthProvider, error) {
	u, err := url.Parse(authURL)
	if err != nil {
		return nil, fmt.Errorf("error parsing auth URL: %s", err.Error())
	}

	switch u.Scheme {
	case "imap":
		return NewIMAP(u.Host, false), nil
	case "imaps":
		return NewIMAP(u.Host, true), nil
	default:
		return nil, fmt.Errorf("no auth provider found for %s:// URL", u.Scheme)
	}
}
