# tokidoki

Tokidoki is a (WIP) carddav and caldav server.

## Architecture

Tokidoki relies on [go-webdav][1] for the protocol implementation. It provides
the storage backend and authentication, both of which are easily pluggable.

It currently provides:

Authentication:

* IMAP (working)

Storage:

* Filesystem (working)
* PostgreSQL (planned)

[1]: https://github.com/emersion/go-webdav "go-webdav project page on Github"

## Building

* `cd cmd/tokidoki && go build`

## Running

Using the IMAP authentication backend and the filesystem storage backend
(directory must exist), listening on port 8080 on all interfaces:

* `./tokidoki -addr ":8080" -auth.url imaps://imap.example.com:993 -storage.url file:///path/to/storage -debug`

Tokidoki does not support HTTPS at this time, use a reverse TLS proxy if
needed.

## Usage

Only a single address book and calendar per user are supported at the moment,
this is currently a [limitation in go-webdav][2].

Tokidoki supports calendar and address book auto-discovery via
`/.well-known/caldav` and `/.well-known/carddav` respectively, as defined in
[RFC 6764, section 6][3]. Hence, most clients should be able to discover
resources by just pointing them at the server root.

[2]: https://github.com/emersion/go-webdav/issues/26
[3]: https://datatracker.ietf.org/doc/html/rfc6764#section-6
