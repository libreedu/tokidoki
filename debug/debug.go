package debug

import (
	"log"
)

var (
	Enable = false
)

func Printf(format string, v ...any) {
	if Enable {
		log.Printf("[debug] "+format, v...)
	}
}
