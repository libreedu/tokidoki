module git.sr.ht/~sircmpwn/tokidoki

go 1.18

require (
	github.com/emersion/go-ical v0.0.0-20200224201310-cd514449c39e
	github.com/emersion/go-imap v1.2.0
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac
	github.com/emersion/go-vcard v0.0.0-20210521075357-3445b9171995
	github.com/emersion/go-webdav v0.3.2-0.20220603063605-db966a275c93
	github.com/go-chi/chi/v5 v5.0.7
)

require golang.org/x/text v0.3.7 // indirect
