package storage

import (
	"context"

	"github.com/emersion/go-vcard"
	"github.com/emersion/go-webdav/caldav"
	"github.com/emersion/go-webdav/carddav"
)

type psqlBackend struct{}

var _ carddav.Backend = (*psqlBackend)(nil)

func NewPostgreSQL() (caldav.Backend, carddav.Backend, error) {
	return nil, &psqlBackend{}, nil
}

func (*psqlBackend) CurrentUserPrincipal(ctx context.Context) (string, error) {
	panic("TODO")
}

func (*psqlBackend) AddressbookHomeSetPath(ctx context.Context) (string, error) {
	panic("TODO")
}

func (*psqlBackend) AddressBook(ctx context.Context) (*carddav.AddressBook, error) {
	panic("TODO")
}

func (*psqlBackend) GetAddressObject(ctx context.Context, path string, req *carddav.AddressDataRequest) (*carddav.AddressObject, error) {
	panic("TODO")
}

func (*psqlBackend) ListAddressObjects(ctx context.Context, req *carddav.AddressDataRequest) ([]carddav.AddressObject, error) {
	panic("TODO")
}

func (*psqlBackend) QueryAddressObjects(ctx context.Context, query *carddav.AddressBookQuery) ([]carddav.AddressObject, error) {
	panic("TODO")
}

func (*psqlBackend) PutAddressObject(ctx context.Context, path string, card vcard.Card, opts *carddav.PutAddressObjectOptions) (loc string, err error) {
	panic("TODO")
}

func (*psqlBackend) DeleteAddressObject(ctx context.Context, path string) error {
	panic("TODO")
}
